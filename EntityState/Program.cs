﻿// tạo person sẽ thêm vào database
using Microsoft.EntityFrameworkCore;

var person = new Person
{
    Name = "A",
};

// khởi tạo đối tượng context đại diện cho 1 database
var context = new AppDbContext();
//Console.WriteLine($"entity state trước khi add {context.Entry(person).State}");

//// đánh dấu là thêm vào database
//context.Persons.AddRange(person);
//Console.WriteLine($"entity state sau khi add {context.Entry(person).State}");

////lưu mọi sự thay đổi
//context.SaveChanges();
//Console.WriteLine($"entity state sau khi saveChanges {context.Entry(person).State}");

//person.Name = "Minh";
//Console.WriteLine($"Update new Minh{context.Entry(person).State}");
//context.SaveChanges();


context.Persons.AsNoTracking().First();
context.Entry(person).State = Microsoft.EntityFrameworkCore.EntityState.Added;
var count = context.SaveChanges();
Console.WriteLine(count);


