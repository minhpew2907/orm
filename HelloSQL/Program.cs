﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

Console.WriteLine("hello");

public class AppDbContext : DbContext
{
    public DbSet<Person> Persons { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString = "Data Source=LAPTOP-NR588SC6\\SQLEXPRESS;Database=HelloSQL;Trusted_Connection=True;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connectionString);
    }
}


public class Person
{
    [Key]
    public int Id { get; set; }

    [Required]
    public string Name { get; set; }
}
