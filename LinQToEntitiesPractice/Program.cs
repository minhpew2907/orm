﻿// khởi tạo đối tượng context đại diện cho 1 database
using Newtonsoft.Json;

var context = new AppDbContext();

// tạo danh sách Product sẽ thêm vào database
var s = new StreamReader("MOCK_DATA.json");
var jr = new JsonTextReader(s);
var js = new JsonSerializer();
var obj = js.Deserialize<List<Product>>(jr);
// đánh dấu là thêm vào database
context.Products.AddRange(obj);

// lưu mọi sự thay đổi
context.SaveChanges();
var number = context.Products.Count();
Console.WriteLine($"number of record is: {number}");

//Lấy danh sách tất cả các sản phẩm.
var product = context.Products.ToList();
foreach (var item in product)
{
    Console.WriteLine($"\nId: {item.Id} - Name: {item.Name} - Price: {item.Price} - Stock Quantity: {item.StockQuantity} - Create at: {item.CreatedAt} - IsActive: {item.IsActive}");
}

//Lấy danh sách tên của tất cả các sản phẩm.
var productName = context.Products.Select(p => p.Name).ToList();
Console.WriteLine($"2. Name of product: {string.Join(" \n ", productName)}");

//Lấy danh sách các sản phẩm có giá lớn hơn 50 đồng.
var productPrice = context.Products.Where(p => p.Price > 50).ToList();
foreach (var item in productPrice)
{
    Console.WriteLine($"Product name: {item.Name} - Product price: {item.Price}");
}

//Lấy danh sách các sản phẩm có số lượng tồn kho ít hơn 10.
var productStockQuantity = context.Products.Where(p => p.StockQuantity < 10).ToList();
foreach (var item in productStockQuantity)
{
    Console.WriteLine($"Product name: {item.Name} - In Stock: {item.Name}");
}

////Lấy danh sách tên của các sản phẩm có giá từ 100 đến 500 đồng.
var productNamePrice100to500 = context.Products.Where(p => p.Price >= 100 && p.Price <= 500).ToList();
foreach (var item in productNamePrice100to500)
{
    Console.WriteLine($"Product name: {item.Name} - Price: {item.Price}");
}

//Lấy danh sách các sản phẩm được tạo ra trong vòng 7 ngày gần đây.
var date = DateTime.Now - TimeSpan.FromDays(7);
var productDate = context.Products.Select(p => new { p.Name, p.CreatedAt }).Where(p => p.CreatedAt <= date).ToList();
foreach (var item in productDate)
{
    Console.WriteLine($"Product name: {item.Name} - Create at: {item.CreatedAt}");
}

//Lấy danh sách các sản phẩm không còn tồn tại (không hoạt động).
var productNotExist = context.Products.Where(p => p.IsActive == false).ToList();
foreach (var item in productNotExist)
{
    Console.WriteLine($"Product name: {item.Name} - Is Active: {item.IsActive}");
}

//Lấy tổng số lượng tồn kho của tất cả sản phẩm.
var sumOfProductInstock = context.Products.Select(p => p.StockQuantity).Sum();
Console.WriteLine($"Product in stock: {sumOfProductInstock}");

//Lấy tên của sản phẩm và ngày tạo của sản phẩm có giá lớn hơn 200 đồng.
var nameDateOfProduct = context.Products.Select(p => new { p.Name, p.CreatedAt, p.Price }).Where(p => p.Price > 200).ToList();
foreach (var item in nameDateOfProduct)
{
    Console.WriteLine($"\nName: {item.Name} - Create at: {item.CreatedAt} - Price: {item.Price}");
}

//Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
var productPriceLessThan = context.Products.OrderByDescending(p => p.Price);
foreach (var item in productPriceLessThan)
{
    Console.WriteLine($"Name: {item.Name} - Price less than: {item.Price}");
}

//Order By:

//Lấy danh sách các sản phẩm được sắp xếp theo tên sản phẩm tăng dần.
var nameOfProductAscending = context.Products.OrderBy(p => p.Name);
foreach (var item in nameOfProductAscending)
{
    Console.WriteLine($"Name: {item.Name}");
}

//Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
var productPriceLessThanMore = context.Products.OrderByDescending(p => p.Price);
foreach (var item in productPriceLessThanMore)
{
    Console.WriteLine($"Name: {item.Name} - Price less than: {item.Price}");
}

//Lấy danh sách các sản phẩm được sắp xếp theo ngày tạo mới nhất đến cũ nhất.
var productDateNewAndOld = context.Products.OrderByDescending(p => p.CreatedAt);
foreach (var item in productDateNewAndOld)
{
    Console.WriteLine($"Name: {item.Name} - Create at: {item.CreatedAt}");
}

//Group By:

//Lấy danh sách các sản phẩm được nhóm theo số lượng tồn kho. (Mỗi nhóm hiển thị số lượng và tên các sản phẩm trong nhóm đó)
var productInStockGroupBy = context.Products.GroupBy(p => p.StockQuantity);
foreach (var group in productInStockGroupBy)
{
    Console.WriteLine($"In Stock: {group.Key}");
    foreach (var item in group)
    {
        Console.WriteLine($"Name: {item.Name}");
    }
}

//Lấy danh sách các sản phẩm được nhóm theo khoảng giá (0-100, 101-200, v.v.). (Mỗi nhóm hiển thị khoảng giá và số lượng sản phẩm trong nhóm đó)
var productPriceGroupBy = context.Products.GroupBy(p => p.Price);
foreach (var group in productPriceGroupBy)
{
    Console.WriteLine($"Price: {group.Key}");
    foreach (var item in group)
    {
        Console.WriteLine($"Product in stock: {item.StockQuantity}");
    }
}

//First, FirstOrDefault, Last, LastOrDefault, Single, SingleOrDefault:

//Lấy thông tin chi tiết của sản phẩm đầu tiên trong danh sách.
var firstProduct = context.Products.First();
Console.WriteLine($"Id: {firstProduct.Id} - Name: {firstProduct.Name} - Price: {firstProduct.Price} - Stock Quantity: {firstProduct.StockQuantity} - Create at: {firstProduct.CreatedAt} - IsActive: {firstProduct.IsActive} ");

//Lấy thông tin chi tiết của sản phẩm có giá cuối cùng.
var lastProduct = context.Products.OrderBy(p => p.Price).Last();
Console.WriteLine($"Id: {lastProduct.Id} - Name: {lastProduct.Name} - Price: {lastProduct.Price} - Stock Quantity: {lastProduct.StockQuantity} - Create at: {lastProduct.CreatedAt} - IsActive: {lastProduct.IsActive} ");

//Lấy thông tin chi tiết của sản phẩm có tên là "iPhone" (nếu có).
var iphoneProduct = context.Products.SingleOrDefault(p => p.Name.Contains("iPhone"));
if (iphoneProduct is null)
{
    Console.WriteLine("Not found.");
}
else
{
    Console.WriteLine($"Id: {iphoneProduct.Id} - Name: {iphoneProduct.Name} - Price: {iphoneProduct.Price} - Stock Quantity: {iphoneProduct.StockQuantity} - Create at: {iphoneProduct.CreatedAt} - IsActive: {iphoneProduct.IsActive} ");
}

//Lấy thông tin chi tiết của sản phẩm có ID là 5 (nếu có).
var productIdIs5 = context.Products.SingleOrDefault(p => p.Id == 5);
if (productIdIs5 is null)
{
    Console.WriteLine("Not found.");
}
else
{
    Console.WriteLine($"Id: {productIdIs5.Id} - Name: {productIdIs5.Name} - Price: {productIdIs5.Price} - Stock Quantity: {productIdIs5.StockQuantity} - Create at: {productIdIs5.CreatedAt} - IsActive: {productIdIs5.IsActive} ");
}

//Lấy thông tin chi tiết của sản phẩm có giá là 100 (nếu có).
var productPriceIs100 = context.Products.SingleOrDefault(p => p.Price == 100);
if (productPriceIs100 is null)
{
    Console.WriteLine("Not found.");
}
else
{
    Console.WriteLine($"Id: {productPriceIs100.Id} - Name: {productPriceIs100.Name} - Price: {productPriceIs100.Price} - Stock Quantity: {productPriceIs100.StockQuantity} - Create at: {productPriceIs100.CreatedAt} - IsActive: {productPriceIs100.IsActive} ");
}
//Paging:

//Lấy danh sách 10 sản phẩm đầu tiên trong danh sách.
var firstProductPage = context.Products.Skip(0).Take(10);
foreach (var item in firstProductPage)
{
    Console.WriteLine($"\nId: {item.Id} - Name: {item.Name} - Price: {item.Price} - Stock Quantity: {item.StockQuantity} - Create at: {item.CreatedAt} - IsActive: {item.IsActive}");
}
//Lấy danh sách 20 sản phẩm từ vị trí thứ 21 trong danh sách.
var secondProductPage = context.Products.Skip(20).Take(20);
foreach (var item in secondProductPage)
{
    Console.WriteLine($"\nId: {item.Id} - Name: {item.Name} - Price: {item.Price} - Stock Quantity: {item.StockQuantity} - Create at: {item.CreatedAt} - IsActive: {item.IsActive}");
}
